package tercerparcial;
public class nodo {
   private N value;
    private nodo<N> next;
    
    public nodo(N resp, nodo<N> m){
        this.value = resp;
        this.next = m;
    }

    public N getValue() { 
        return value;
    }

    public void setValue(N value) {
        this.value = value;
    }

    public nodo<N> getNext() {
        return next;
    }

    public void setNext(nodo<N> next) {
        this.next = next;
    }
    
}
