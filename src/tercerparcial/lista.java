package tercerparcial;
import java.util.Iterator;
public class lista <N> implements Iterable<T>
        private nodo<N> fr;
    private nodo<N> ls;
    private int size;
    
    public listadinamica(){
        this.fr = null;
        this.ls = null;
        this.size = 0;
    }
    
    public boolean isEmpty(){
        return size==0;
    }
    
    public int sizeList(){
        return size;
    }
    
    private nodo<N> getNode(int ind){
        if(isEmpty() || (ind<0 || ind >=sizeList())){
            return null;
        }else if(ind == 0){
            return first;
        }else if(ind == sizeList() -1){
            return last;
        }else{
            nodo<N> searchNodo = first;
            int count = 0;
            while(count < ind){
                count++;
                searchNodo = searchNodo.getNext();
            }
            return searchNodo;
        }
    }
    
    public N get(int ind){
        if(isEmpty() || (index<0 || index >=sizeList())){
            return null;
        }else if(ind == 0){
            return fr.getValue();
        }else if(ind == sizeList() -1){
            return last.getValue();
        }else{
            nodo<N> searchNodeValue = getNode(ind);
            return searchNodeValue.getValue();
        }
    }
    
    public N getFr(){
        if(isEmpty()){
            return null;
        }else{
            return fr.getValue();
        }
    }
    
    public N getLast(){
        if(isEmpty()){
            return null;
        }else{
            return last.getValue();
        }
    }
    
    
    public N addFr(N value){
        nodo<N> newvalue;
        if(isEmpty()){
            newvalue = new nodo<>(value,null);
            fr = newvalue;
            ls = newvalue;
        }else{
            newvalue = new nodo<>(value,first);
            fr = newvalue;
        }
        size++;
        return fr.getValue();
    }
    public N addLast(N value){
        nodo<N> newvalue;
        if(isEmpty()){
            return addFirst(value);
        }else{
            newvalue = new nodo<>(value,null);
            ls.setNext(newvalue);
            ls = newvalue;
        }
        size++;
        return ls.getValue();
    }
    
    public N add(N value,int ind){
        if(ind == 0){
            return addFr(value);
        }else if(ind == sizeList()){
            return addLast(value);
        }else if((ind < 0 || ind >= sizeList()-1)){
            return null;
        }else{
            nodo<N> nodo_prev = getNode(ind-1);
            nodo<N> nodo_current = getNode(ind);
            nodo<N> newvalue = new nodo<>(value,nodo_current);
            nodo_prev.setNext(newvalue);
            size++;
            return getNode(ind).getValue();
        }
    }
    
    @Override
    public Iterator<N> iterator() {
        throw new UnsupportedOperationException("Not supported yet."); 
    }
  
}
